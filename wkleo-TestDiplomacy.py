#!/usr/bin/env python3

# -------------------------------
# projects/diplomacy/TestDiplomacy.py
# -------------------------------


# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import read_input, get_support, city_army, action, diplomacy_eval, diplomacy_print, diplomacy_solve

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("A Madrid Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\n")

    def test_solve_2(self):
        r = StringIO("A Madrid Move London\nB London Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\n")

    def test_solve_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")
            
    def test_solve_4(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Barcelona\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC Barcelona\n")
            
    def test_solve_5(self):
        r = StringIO("A Barcelona Move Madrid\nB London Move Barcelona\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\nB Barcelona\n")
            
    def test_solve_6(self):
        r = StringIO("A Madrid Hold\nB Paris Hold\nC Moscow Move Madrid\nD Kiev Support C\nE Berlin Move Paris\nF Austin Support E\nG Houston Support F\nH Dallas Support F\nI Copenhagen Move Austin\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC Madrid\nD Kiev\nE [dead]\nF Austin\nG Houston\nH Dallas\nI [dead]\n")
    
    def test_solve_7(self):
        r = StringIO("A Berlin Support B\nB Madrid Hold\nC Detroit Move London\nD London Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Berlin\nB Madrid\nC London\nD [dead]\n")
    


# ----
# main
# ----


if __name__ == "__main__":#pragma: no cover
    main()
